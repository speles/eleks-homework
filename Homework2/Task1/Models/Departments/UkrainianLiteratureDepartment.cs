﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Models.Departments
{
    class UkrainianLiteratureDepartment: Department
    {
        public UkrainianLiteratureDepartment() : base("Ukrainian Literature")
        {
        }
    }
}
