﻿using System;
using System.Collections.Generic;
using Task1.Interfaces;

namespace Task1.Models
{
    class Author : IComparable<Author>, ICountingBooks
    {
        public static readonly Author UnknownAuthor = new Author("Unknown", "author");

        public string Name { get; private set; }
        public string Surname { get; private set; }

        public List<Book> Books = new List<Book>();

        public int BooksCount
        {
            get
            {
                return Books.Count;
            }
        }

        public Author() : this("Unnamed", "author")
        {
        }

        public Author(string name, string surname)
        {
            Name = name;
            Surname = surname;
            Books = new List<Book>();
        }

        public override string ToString()
        {
            return $"{Name} {Surname}";
        }

        public int CompareTo(Author other)
        {
            return BooksCount.CompareTo(other.BooksCount);
        }

        public void AddBook(Book book)
        {
            Books.Add(book);
        }
    }
}
