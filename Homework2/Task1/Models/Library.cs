﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task1.Interfaces;

namespace Task1.Models
{
    class Library: ICountingBooks, IEnumerable<Department>
    {
        List<Department> _departments;

        public string Name { get; private set; }
        public string Address { get; private set; }

        public Library(string name = "Unnamed", string address = "Location unknown"): this(name, address, new Department[0])
        {
        }

        public Library(string name, string adress, IEnumerable<Department> departments)
        {
            Name = name;
            Address = adress;
            _departments = new List<Department>(departments);
        }

        public int BooksCount
        {
            get
            {
                int sum = 0;
                foreach (var department in _departments)
                {
                    sum += department.BooksCount;
                }
                return sum;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"\"{Name}\" Library ({Address}):");
            foreach (var department in _departments)
            {
                sb.AppendLine($"\t{department}:");
                foreach (var book in department)
                {
                    sb.AppendLine($"\t\t{book}");
                }
            }
            return sb.ToString();
        }

        public Book GetBookByTitlePrefix(string titlePrefix)
        {
            foreach (var department in _departments)
            {
                var foundBook = department.FirstOrDefault(book => book.Title.StartsWith(titlePrefix));
                if (foundBook != null)
                {
                    return foundBook;
                }
            }
            return null;
        }

        public void SortBooksByTitle()
        {
            _departments.ForEach(department => department.SortBooksByTitle());
        }

        public Department GetDepartmentWithMaximumBooks()
        {
            Department result = null;
            foreach (var department in _departments)
            {
                if ((result == null) || (result.BooksCount < department.BooksCount))
                {
                    result = department;
                }
            }
            return result;
        }

        public Book GetBookWithMinimumPages()
        {
            Book result = null;
            foreach (var department in _departments)
            {
                foreach (var book in department)
                {
                    if ((result == null) || (result.PagesCount > book.PagesCount))
                    {
                        result = book;
                    }
                }
            }
            return result;
        }

        public Author GetAuthorWithMaximumBooks()
        {
            Author result = null;

            foreach (var department in _departments)
            {
                Author departmentResult = null;
                foreach (var book in department)
                {
                    if ((departmentResult == null) || (departmentResult.BooksCount < book.Author.BooksCount))
                    {
                        departmentResult = book.Author;
                    }
                }
                if ((result == null) || (result.BooksCount < departmentResult.BooksCount))
                {
                    result = departmentResult;
                }
            }
            return result;
        }

        public void AddDepartment(Department department)
        {
            _departments.Add(department);
        }

        public IEnumerator<Department> GetEnumerator()
        {
            return ((IEnumerable<Department>)_departments).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Department>)_departments).GetEnumerator();
        }
    }
}
