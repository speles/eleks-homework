﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Task1.Interfaces;
using Task1.Models;

namespace Task1.Providers
{
    class XmlLibraryProvider
    {
        static public Library LoadLibrary(string filename)
        {
            XDocument xDoc = XDocument.Load(filename);
            if ((xDoc != null) && xDoc.Root.HasElements)
            {
                XElement xBookLibrary = xDoc.Element("BooksLibrary");

                string libraryName = xBookLibrary.Attribute("name").Value;
                string libraryAddress = xBookLibrary.Attribute("address").Value;

                List<Department> departments = new List<Department>();
                Dictionary<int, Author> authors = new Dictionary<int, Author>();

                XElement xAuthors = xBookLibrary.Element("Authors");
                foreach (var xAuthor in xAuthors.Elements())
                {
                    int authorId = Convert.ToInt32(xAuthor.Attribute("id").Value);
                    string authorName = xAuthor.Attribute("name").Value;
                    string authorSurname = xAuthor.Attribute("surname").Value;

                    authors[authorId] = new Author(authorName, authorSurname);
                }

                XElement xDepartments = xBookLibrary.Element("Departments");
                foreach (var xDepartment in xDepartments.Elements())
                {
                    List<Book> books = new List<Book>();
                    foreach (var xBook in xDepartment.Elements())
                    {
                        string bookTitle = xBook.Attribute("title").Value;
                        int pagesCount = Convert.ToInt32(xBook.Attribute("pages").Value);
                        int authorId = Convert.ToInt32(xBook.Attribute("author").Value);
                        books.Add(new Book(bookTitle, pagesCount, authors[authorId]));
                    }
                    string departmentTitle = xDepartment.Attribute("title").Value;
                    departments.Add(new Department(departmentTitle, books));
                }
                return new Library(libraryName, libraryAddress, departments);
            }
            return null;                 
        }
        
        static public void SaveLibrary(Library library, string filename)
        {
            XDocument xDoc = new XDocument();
            XElement xBookLibrary = new XElement("BooksLibrary");
            xBookLibrary.SetAttributeValue("name", library.Name);
            xBookLibrary.SetAttributeValue("address", library.Address);

            Dictionary<Author, int> authorsId = new Dictionary<Author, int>();

            XElement xDepartments = new XElement("Departments");
            foreach (var department in library)
            {
                XElement xDepartment = new XElement("Department");
                xDepartment.SetAttributeValue("title", department.Title);
                foreach (var book in department)
                {
                    XElement xBook = new XElement("Book");

                    xBook.SetAttributeValue("title", book.Title);
                    xBook.SetAttributeValue("pages", book.PagesCount);
                    if (!authorsId.ContainsKey(book.Author))
                    {
                        authorsId[book.Author] = authorsId.Count;
                    }
                    xBook.SetAttributeValue("author", authorsId[book.Author]);
                    xDepartment.Add(xBook);
                }
                xDepartments.Add(xDepartment);
            }

            XElement xAuthors = new XElement("Authors");
            foreach (var authorKvp in authorsId)
            {
                XElement xAuthor = new XElement("Author");

                xAuthor.SetAttributeValue("name", authorKvp.Key.Name);
                xAuthor.SetAttributeValue("surname", authorKvp.Key.Surname);
                xAuthor.SetAttributeValue("id", authorKvp.Value);

                xAuthors.Add(xAuthor);
            }

            xBookLibrary.Add(xAuthors);
            xBookLibrary.Add(xDepartments);

            xDoc.Add(xBookLibrary);
            xDoc.Save(filename);
        }
    }
}
