﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix matrix;
            Console.WriteLine("Press i to fill matrix from console.");
            Console.WriteLine("Press any onther key to fill using random.");
            var key = Console.ReadKey();
            Console.WriteLine();
            if (key.KeyChar == 'i')
            {
                matrix = new Matrix();
            } else
            {
                matrix = new Matrix(10, 10, -2, 20);
            }
            Console.WriteLine($"Input matrix:");
            Console.WriteLine(matrix);
            Console.WriteLine();
            int?[] negativeRowsSums = matrix.GetSumOfRowsWithNegativeElements();
            for (int row = 0; row < negativeRowsSums.Length; row++)
            {
                Console.Write($"Row #{row + 1} has ");
                if (negativeRowsSums[row].HasValue)
                {
                    Console.WriteLine("sum = " + negativeRowsSums[row].Value);
                } else
                {
                    Console.WriteLine("no negative elements");
                }
            }
            Console.WriteLine();
            var saddlePoints = matrix.GetSaddlePoints();
            if (saddlePoints.Length > 0)
            {
                Console.WriteLine("Saddle points:");
                foreach (var saddlePoint in saddlePoints)
                {
                    Console.WriteLine($"Row #{saddlePoint.Item1 + 1}, column #{saddlePoint.Item2 + 1}");
                }
            } else
            {
                Console.WriteLine("Matrix has no saddle points.");
            }
            Console.ReadKey();

        }
    }
}
