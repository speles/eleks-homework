﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Matrix
    {
        int[][] data;
        public int this[int i, int j]
        {
            get
            {
                return data[i][j];
            }
            set
            {
                data[i][j] = value;
            }
        }
        public int Height
        {
            get
            {
                return data.Length;
            }
        }
        public int Width
        {
            get
            {
                if (data.Length != 0)
                {
                    return data[0].Length;
                }
                else
                {
                    return 0;
                }
            }
        }
        public Matrix(int height, int width, int minRandomValue, int maxRandomValue)
        {
            data = new int[height][];
            Random random = new Random();
            for (int row = 0; row < height; row++)
            {
                data[row] = new int[width];
                for (int col = 0; col < width; col++)
                {
                    data[row][col] = random.Next(minRandomValue, maxRandomValue + 1);
                }
            }
        }
        public Matrix()
        {
            int height, width;

            Console.WriteLine("Enter matrix height:");
            height = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter matrix width:");
            width = Convert.ToInt32(Console.ReadLine());
            data = new int[height][];
            Console.WriteLine($"Enter matrix elements ({height} rows with {width} elements in each):");
            for (int row = 0; row < height; row++)
            {
                data[row] = new int[width];
                var split = Console.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length != width)
                {
                    throw new FormatException("Wrong elements count in row");
                }
                for (int col = 0; col < width; col++)
                {
                    data[row][col] = Convert.ToInt32(split[col]);
                }
            }
        }
        public int?[] GetSumOfRowsWithNegativeElements()
        {
            int?[] result = new int?[Height];
            for (int row = 0; row < Height; row++)
            {
                int sum = 0;
                bool hasNegative = false;
                for (int col = 0; col < Width; col++)
                {
                    sum += data[row][col];
                    if (data[row][col] < 0)
                    {
                        hasNegative = true;
                    }
                }
                if (hasNegative)
                {
                    result[row] = sum;
                }
            }
            return result;
        }
        public Tuple<int, int>[] GetSaddlePoints()
        {
            int[] rowMinimum = new int[Height];
            for (int row = 0; row < Height; row++)
            {
                rowMinimum[row] = int.MaxValue;
                for (int col = 0; col < Width; col++)
                {
                    if (data[row][col] < rowMinimum[row])
                    {
                        rowMinimum[row] = data[row][col];
                    }
                }
            }
            int[] colMaximum = new int[Width];
            for (int col = 0; col < Width; col++)
            {
                colMaximum[col] = int.MinValue;
                for (int row = 0; row < Height; row++)
                {
                    if (data[row][col] > colMaximum[col])
                    {
                        colMaximum[col] = data[row][col];
                    }
                }
            }
            List<Tuple<int, int>> saddlePoints = new List<Tuple<int, int>>();
            for (int row = 0; row < Height; row++)
            {
                for (int col = 0; col < Width; col++)
                {
                    if ((data[row][col] == rowMinimum[row]) && (data[row][col] == colMaximum[col]))
                    {
                        saddlePoints.Add(new Tuple<int, int>(row, col));
                    }
                }
            }
            return saddlePoints.ToArray();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int row = 0; row < Height; row++)
            {
                if (row != 0)
                {
                    sb.AppendLine();
                }
                for (int col = 0; col < Width; col++)
                {
                    if (col != 0)
                    {
                        sb.Append(",\t");
                    }
                    sb.Append(data[row][col]);
                }
            }
            return sb.ToString();
        }
    }
}
