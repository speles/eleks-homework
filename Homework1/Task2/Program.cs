﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix matrix = new Matrix(10, 10);
            Console.WriteLine($"Input matrix:");
            Console.WriteLine(matrix);
            Console.WriteLine();
            Console.WriteLine($"First column without negative integers is {matrix.FirstOnlyPositiveColumn() + 1}");
            Console.WriteLine();
            Console.WriteLine($"Matrix with rows sorted by count of non-unique elements:");
            matrix.SortRowsByNonUniqueCount();
            Console.WriteLine(matrix);
            Console.ReadKey();
        }
    }
}
