﻿namespace Task1.Interfaces
{
    interface ICountingBooks
    {
        int BooksCount
        {
            get;
        }
    }
}
