﻿using System;
using System.Collections;
using System.Collections.Generic;
using Task1.Interfaces;

namespace Task1.Models
{

    abstract class Department: IEnumerable<Book>, IComparable<Department>, ICountingBooks
    {
        Book[] _books;

        public string Title { get; protected set; }

        protected Department(string title) : this(title, new Book[0])
        {
        }

        protected Department (string title, Book[] books)
        {
            Title = title;
            _books = books;
        }

        public int BooksCount
        {
            get
            {
                return _books.Length;
            }
        }

        public void AddBook(Book book)
        {
            Array.Resize(ref _books, _books.Length + 1);
            _books[_books.Length - 1] = book;
        }

        public void RemoveBookAt(int index)
        {
            if ((index < 0) || (index >= _books.Length))
            {
                throw new IndexOutOfRangeException();
            }
            for (int i = index + 1; i < _books.Length; i++)
            {
                _books[i - 1] = _books[i]; 
            }
            Array.Resize(ref _books, _books.Length - 1);
        }

        public int CompareTo(Department other)
        {
            return BooksCount.CompareTo(other.BooksCount);
        }

        public override string ToString()
        {
            return $"{Title} Department";
        }

        public IEnumerator<Book> GetEnumerator()
        {
            return ((IEnumerable<Book>)_books).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Book>)_books).GetEnumerator();
        }
    }
}
