﻿using System;
using Task1.Interfaces;

namespace Task1.Models
{
    class Author: IComparable<Author>, ICountingBooks
    {
        public static readonly Author UnknownAuthor = new Author("Unknown", "author");

        public string Name { get; private set; }
        public string Surname { get; private set; }

        Book[] _books;
        public Book[] Books
        {
            get
            {
                return _books;
            }
        }

        public int BooksCount
        {
            get
            {
                return Books.Length;
            }
        }

        public Author(): this("Unnamed", "author")
        {
        }

        public Author(string name, string surname)
        {
            Name = name;
            Surname = surname;
            _books = new Book[0];
        }

        public override string ToString()
        {
            return $"{Name} {Surname}";
        }

        public int CompareTo(Author other)
        {
            return BooksCount.CompareTo(other.BooksCount);
        }

        public void AddBook(Book book)
        {
            Array.Resize(ref _books, _books.Length + 1);
            _books[_books.Length - 1] = book;
        }
    }
}
