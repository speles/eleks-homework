﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Matrix8x8
    {
        public const int SIZE = 8;
        int[][] data;
        public int this[int i, int j]
        {
            get
            {
                return data[i][j];
            }
            set
            {
                data[i][j] = value;
            }
        }
        public int Height
        {
            get
            {
                return data.Length;
            }
        }
        public int Width
        {
            get
            {
                if (data.Length != 0)
                {
                    return data[0].Length;
                }
                else
                {
                    return 0;
                }
            }
        }
        public Matrix8x8(int minRandomValue, int maxRandomValue)
        {
            int height = 8, width = 8;
            data = new int[height][];
            Random random = new Random();
            for (int row = 0; row < height; row++)
            {
                data[row] = new int[width];
                for (int col = 0; col < width; col++)
                {
                    data[row][col] = random.Next(minRandomValue, maxRandomValue + 1);
                }
            }
        }
        public Matrix8x8()
        {
            int height = SIZE, width = SIZE;
            data = new int[height][];
            Console.WriteLine($"Enter matrix elements ({height} rows with {width} elements in each):");
            for (int row = 0; row < height; row++)
            {
                data[row] = new int[width];
                var split = Console.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length != width)
                {
                    throw new FormatException("Wrong elements count in row");
                }
                for (int col = 0; col < width; col++)
                {
                    data[row][col] = Convert.ToInt32(split[col]);
                }
            }
        }
        public int?[] GetSumOfRowsWithNegativeElements()
        {
            int?[] result = new int?[Height];
            for (int row = 0; row < Height; row++)
            {
                int sum = 0;
                bool hasNegative = false;
                for (int col = 0; col < Width; col++)
                {
                    sum += data[row][col];
                    if (data[row][col] < 0)
                    {
                        hasNegative = true;
                    }
                }
                if (hasNegative)
                {
                    result[row] = sum;
                }
            }
            return result;
        }

        public bool IsRowSameAsColumn(int index)
        {
            if ((index < 0) || (index >= SIZE))
                throw new ArgumentOutOfRangeException();
            for (int i = 0; i < SIZE; i++)
            {
                if (data[index][i] != data[i][index])
                {
                    return false;
                }
            }
            return true;
        }
        

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int row = 0; row < Height; row++)
            {
                if (row != 0)
                {
                    sb.AppendLine();
                }
                for (int col = 0; col < Width; col++)
                {
                    if (col != 0)
                    {
                        sb.Append(",\t");
                    }
                    sb.Append(data[row][col]);
                }
            }
            return sb.ToString();
        }
    }
}
