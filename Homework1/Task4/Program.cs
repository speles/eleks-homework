﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix8x8 matrix;
            Console.WriteLine("Press i to fill matrix from console.");
            Console.WriteLine("Press any onther key to fill using random.");
            var key = Console.ReadKey();
            Console.WriteLine();
            if (key.KeyChar == 'i')
            {
                matrix = new Matrix8x8();
            }
            else
            {
                matrix = new Matrix8x8(-2, 20);
            }
            Console.WriteLine($"Input matrix:");
            Console.WriteLine(matrix);
            Console.WriteLine();
            for (int k = 0; k < Matrix8x8.SIZE; k++)
            {
                if (matrix.IsRowSameAsColumn(k))
                {
                    Console.WriteLine($"Row #{k} is same as column #{k}");
                }
            }
            Console.WriteLine();
            int?[] negativeRowsSums = matrix.GetSumOfRowsWithNegativeElements();
            for (int row = 0; row < negativeRowsSums.Length; row++)
            {
                Console.Write($"Row #{row + 1} has ");
                if (negativeRowsSums[row].HasValue)
                {
                    Console.WriteLine("sum = " + negativeRowsSums[row].Value);
                }
                else
                {
                    Console.WriteLine("no negative elements");
                }
            }
            Console.ReadKey();
        }
    }
}
