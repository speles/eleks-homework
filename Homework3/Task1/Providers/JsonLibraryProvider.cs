﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task1.Models;

namespace Task1.Providers
{
    class JsonLibraryProvider
    {
        static public Library LoadLibrary(string filename)
        {
            try
            {
                string jsonString = File.ReadAllText(filename);
                DefaultContractResolver dcr = new DefaultContractResolver();
                dcr.DefaultMembersSearchFlags |= System.Reflection.BindingFlags.NonPublic;
                return JsonConvert.DeserializeObject<Library>(
                    jsonString, 
                    new JsonSerializerSettings
                    {
                        ContractResolver = dcr,
                        ObjectCreationHandling = ObjectCreationHandling.Replace
                    });
            }
            catch (Exception e)
            {
                Console.WriteLine("Error deserializing: " + e);
                return null;
            }
        }

        static public void SaveLibrary(Library library, string filename)
        {
            try
            {
                DefaultContractResolver dcr = new DefaultContractResolver();
                dcr.DefaultMembersSearchFlags |= System.Reflection.BindingFlags.NonPublic;

                string jsonString = JsonConvert.SerializeObject(library, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ContractResolver = dcr,
                        ObjectCreationHandling = ObjectCreationHandling.Replace
                    });
                File.WriteAllText(filename, jsonString);
            } catch (Exception e)
            {
                Console.WriteLine("Error serializing: " + e.StackTrace);
            }
        }
    }
}
