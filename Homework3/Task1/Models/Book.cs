﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Models
{
    class Book: IComparable<Book>
    {
        public int PagesCount { get; private set; }
        public string Title { get; private set; }
        public Author Author { get; private set; }

        public Book(): this("Untitled", 0)
        {
        }

        public Book(string title, int pagesCount): this(title, pagesCount, Author.UnknownAuthor)
        {
        }

        public Book(string title, int pagesCount, Author author)
        {
            Title = title;
            Author = author;
            PagesCount = pagesCount;
            Author.AddBook(this);
        }

        public int CompareTo(Book other)
        {
            return PagesCount.CompareTo(other.PagesCount);
        }

        public override string ToString()
        {
            return $"\"{Title}\" ({Author}, {PagesCount} pages)";
        }
    }
}
