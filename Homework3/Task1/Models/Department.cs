﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using Task1.Interfaces;

namespace Task1.Models
{
    [JsonObject]
    class Department: IComparable<Department>, IEnumerable<Book>, ICountingBooks
    {
        private List<Book> _books { get; set; }

        public string Title { get; protected set; }

        [JsonIgnore]
        public int BooksCount
        {
            get
            {
                return _books.Count;
            }
        }

        public Department() : this("Unnamed")
        {
        }

        public Department(string title) : this(title, new Book[0])
        {
        }

        public Department (string title, IEnumerable<Book> books)
        {
            Title = title;
            _books = new List<Book>(books);
        }

        public void AddBook(Book book)
        {
            _books.Add(book);
        }

        public void RemoveBookAt(int index)
        {
            _books.RemoveAt(index);
        }

        public int CompareTo(Department other)
        {
            return BooksCount.CompareTo(other.BooksCount);
        }

        public override string ToString()
        {
            return $"{Title} Department";
        }

        public void SortBooksByTitle()
        {
            _books.Sort((book1, book2) => book1.Title.CompareTo(book2.Title));
        }

        public IEnumerator<Book> GetEnumerator()
        {
            return ((IEnumerable<Book>)_books).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _books.GetEnumerator();
        }
    }
}
