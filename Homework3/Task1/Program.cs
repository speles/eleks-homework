﻿using System;
using System.Text;
using Task1.Models;
using Task1.Models.Departments;
using Task1.Providers;

namespace Task1
{
    class Program
    {
        static Department CreateITDepartment()
        {
            Author gayleMcdowell = new Author("Gayle", "McDowell");
            Author robertMartin = new Author("Robert", "Martin");
            Author stevenSkiena = new Author("Steven", "Skiena");
            Author kennethRubin = new Author("Kenneth", "Rubin");
            Author steveMcconnel = new Author("Steve", "McConnell");

            Book crackingInterview = new Book("Cracking the Coding Interview", 687, gayleMcdowell);
            Book cleanCode = new Book("Clean Code", 464, robertMartin);
            Book algorithmDesign = new Book("The Algorithm Design Manual", 730, stevenSkiena);
            Book essentialScrum = new Book("Essential Scrum", 504, kennethRubin);
            Book codeComplete = new Book("Code Complete", 960, steveMcconnel);

            var itDepartment = new ItDepartment();
            itDepartment.AddBook(crackingInterview);
            itDepartment.AddBook(cleanCode);
            itDepartment.AddBook(algorithmDesign);
            itDepartment.AddBook(essentialScrum);
            itDepartment.AddBook(codeComplete);

            return itDepartment;
        }

        static Department CreateWorldLiteratureDepartment()
        {
            Author lewisCarrol = new Author("Lewis", "Carroll");
            Author josephHeller = new Author("Joseph", "Heller");
            Author roalsDahl = new Author("Roald", "Dahl");
            Author rayBradburry = new Author("Ray", "Bradbury");
            Author georgeOrwell = new Author("George", "Orwell");

            Book alicesAdventure = new Book("Alice's Adventures in Wonderland", 272, lewisCarrol);
            Book catch22 = new Book("Catch-22", 544, josephHeller);
            Book charlieAndChocolate = new Book("Charlie and the Chocolate Factory", 176, roalsDahl);
            Book fahrenheit451 = new Book("Fahrenheit 451", 249, rayBradburry);
            Book book1984 = new Book("1984", 328, georgeOrwell);

            var worldDepartment = new WorldLiteratureDepartment();
            worldDepartment.AddBook(alicesAdventure);
            worldDepartment.AddBook(catch22);
            worldDepartment.AddBook(charlieAndChocolate);
            worldDepartment.AddBook(fahrenheit451);
            worldDepartment.AddBook(book1984);

            return worldDepartment;
        }

        static Department CreateUkrainianLiteratureDepartment()
        {
            Author ivanBahrianyi = new Author("Ivan", "Bahrianyi");
            Author lesyaUkrainka = new Author("Lesya", "Ukrainka");
            Author tarasShevchenko = new Author("Taras", "Shevchenko");
            Author mykhailoKotsiubynsky = new Author("Mykhailo", "Kotsiubynsky");
            Author panasMyrny = new Author("Panas", "Myrny");
            Author olexandrDovzhenko = new Author("Olexandr", "Dovzhenko");

            Book tigerCatchers = new Book("Тигролови", 256, ivanBahrianyi);
            Book forestSong = new Book("Лісова пісня", 60, lesyaUkrainka);
            Book kobzar = new Book("Кобзар", 578, tarasShevchenko);
            Book shadowsOfForgotten = new Book("Тіні забутих предків", 237, mykhailoKotsiubynsky);
            Book voly = new Book("Хіба ревуть воли, як ясла повні?", 352, panasMyrny);
            Book ukraineInFire = new Book("Україна в огні", 416, olexandrDovzhenko);
            Book fascinatedDesna = new Book("Зачарована Десна", 287, olexandrDovzhenko);

            var ukrainianDepartment = new UkrainianLiteratureDepartment();
            ukrainianDepartment.AddBook(tigerCatchers);
            ukrainianDepartment.AddBook(forestSong);
            ukrainianDepartment.AddBook(kobzar);
            ukrainianDepartment.AddBook(shadowsOfForgotten);
            ukrainianDepartment.AddBook(voly);
            ukrainianDepartment.AddBook(ukraineInFire);
            ukrainianDepartment.AddBook(fascinatedDesna);

            return ukrainianDepartment;
        }

        static void Main(string[] args)
        {
            //for ukrainian characters in console
            Console.OutputEncoding = Encoding.Unicode;

            Library library = new Library("Stefanyka", "Stefanyka str, Lviv");
            library.AddDepartment(CreateITDepartment());
            library.AddDepartment(CreateUkrainianLiteratureDepartment());
            library.AddDepartment(CreateWorldLiteratureDepartment());

            Console.WriteLine(library);
            Console.WriteLine();
            Console.WriteLine($"Author with maximum books count: {library.GetAuthorWithMaximumBooks()}");
            Console.WriteLine($"Department with maximum books count: {library.GetDepartmentWithMaximumBooks()}");
            Console.WriteLine($"Book with minimum pages count: {library.GetBookWithMinimumPages()}");
            Console.WriteLine($"Book with title starting with \"Alice\": {library.GetBookByTitlePrefix("Alice")}");

            library.SortBooksByTitle();
            Console.WriteLine("Sorted books:");
            Console.WriteLine(library);

            const string libFilename = "library.json";

            Console.WriteLine();
            JsonLibraryProvider.SaveLibrary(library, libFilename);
            library = JsonLibraryProvider.LoadLibrary(libFilename);

            Console.WriteLine("Library loaded form JSON file:");
            Console.WriteLine(library);

            Console.ReadLine();
        }
    }
}
